# install npm dependancies
```
npm i
```
# start nodemon server
```
npm run dev
```
# install sequelize-cli globally
```
npm -g i sequelize-cli
```
# start migration
```
sequelize db:migrate
```