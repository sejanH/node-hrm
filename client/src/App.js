import React from "react";

import "./assets/sass/main.scss";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch
} from "react-router-dom";

import { Provider } from "react-redux";
import store from "./store";

import Login from "./components/Auth/Login.jsx";
import Admin from "./components/Admin";
import Employee from "./components/Employee";
import AdminLayout from "./components/Admin/AdminLayout";
import EmployeeLayout from "./components/Employee/EmployeeLayout";
import AppRoute from "./components/AppRoute";
import AdminEmployee from "./components/Admin/AdminEmployee";

function App() {
  return (
    <div>
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path="/" component={Login} />
            <AppRoute
              exact
              path="/admin"
              component={Admin}
              layout={AdminLayout}
            />
            <AppRoute
              exact
              path="/admin/employee"
              component={AdminEmployee}
              layout={AdminLayout}
            />
            <AppRoute
              exact
              path="/employee"
              component={Employee}
              layout={EmployeeLayout}
            />
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}

export default App;
