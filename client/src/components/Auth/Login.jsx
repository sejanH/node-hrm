import React, { Component } from "react";
import { connect } from "react-redux";
import { loginUser } from "../../actions/authActions";

export class Login extends Component {
  state = {
    email: "",
    password: "",
    errors: {}
  };
  submitHandler = e => {
    e.preventDefault();
    const newUser = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(newUser);
  };
  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      if (nextProps.auth.user.role == 1) {
        this.props.history.push("/admin");
      } else {
        this.props.history.push("/employee");
      }
    }
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }
  render() {
    const { errors } = this.state;
    return (
      <div className="login-page">
        <div className="login_content p-5">
          {errors.error}
          <h5 className="text-center"> LOG IN YOUR ACCOUNT</h5>
          <div className="form_wrapper">
            <form onSubmit={this.submitHandler}>
              <label className="label">Email Address</label>
              <input
                name="email"
                autoComplete="off"
                className="form-control"
                onChange={this.changeHandler}
                type="email"
                placeholder="Email Address"
              />
              <label className="label">Password </label>
              <input
                name="password"
                autoComplete="off"
                className="form-control"
                onChange={this.changeHandler}
                type="password"
                placeholder="Password"
              />
              <button className="btn submit_btn btn-block mt-2" type="submit">
                Login
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(Login);
