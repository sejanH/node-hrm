import React, { useState } from 'react'
import Sidebar from '../common/Sidebar'
import Navbar from '../common/Navbar'

function AddEmployee() {
    const [isOpen, setIsOpen] = useState(false)
    const [fdfd, dfd] = useState(10)
    const toggle = () => setIsOpen(!isOpen)

    return (
        <div className={isOpen ? "page_wrapper_sm" : "page_wrapper"}>
            <Sidebar></Sidebar>
            <div className="main-content">
                <button onClick={toggle}>btn</button>
                <Navbar>
                </Navbar>
                <div className="content">
                    <h2>Content goes here</h2>
                </div>

            </div>
        </div>
    )
}

export default AddEmployee

