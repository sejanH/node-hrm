import React, { useState, useEffect } from "react";
import EmployeeNavbar from "./EmployeeNavbar";
import EmployeeSidebar from "./EmployeeSidebar";
import "bootstrap/dist/css/bootstrap.css";
import $ from "jquery/dist/jquery";
import "bootstrap/dist/js/bootstrap.bundle";

function EmployeeLayout({ children }) {
  useEffect(() => {
    $("#menu-toggle").click(function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  }, []);
  return (
    <div className="d-flex" id="wrapper">
      <EmployeeSidebar></EmployeeSidebar>
      <EmployeeNavbar children={children}></EmployeeNavbar>
    </div>
  );
}

export default EmployeeLayout;
