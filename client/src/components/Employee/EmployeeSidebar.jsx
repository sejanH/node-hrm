import React from "react";

import logo from "../../assets/images/logo.png";
import Icofont from "react-icofont";
function EmployeeSidebar(props) {
  return (
    <div className="bg-light border-right" id="sidebar-wrapper">
      <div className="sidebar-heading"><img src={logo} /></div>
      <div className="list-group list-group-flush">
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="dashboard" size="2" /> Dashboard
        </a>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="link" size="2" /> Shortcuts
        </a>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="list" size="2" /> Overview
        </a>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="calendar" size="2" /> Events
        </a>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="ui-user" size="2" /> Profile
        </a>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="exclamation-square" size="2" /> Status
        </a>
      </div>
    </div>
  );
}

export default EmployeeSidebar;
