import React, { useState, useEffect } from "react";
import AdminNavbar from "./AdminNavbar";
import AdminSidebar from "./AdminSidebar";
import "bootstrap/dist/css/bootstrap.css";
import $ from "jquery/dist/jquery";
import "bootstrap/dist/js/bootstrap.bundle";

function AdminLayout({ children }) {
  useEffect(() => {
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  }, []);
  return (
    <div className="d-flex" id="wrapper">
      <AdminSidebar />
      <React.Fragment>
        <AdminNavbar children={children}></AdminNavbar>
      </React.Fragment>
    </div>
  );
}

export default AdminLayout;
