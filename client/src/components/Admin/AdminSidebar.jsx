import React from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/images/logo.png";
import Icofont from "react-icofont";
function AdminSidebar(props) {
  return (
    <div className="bg-light border-right" id="sidebar-wrapper">
      <div className="sidebar-heading">
        <img src={logo} />
      </div>
      <div className="list-group list-group">
        <Link
          to="/admin"
          className="list-group-item list-group-item-action bg-light"
        >
          <Icofont className="mr-1" icon="dashboard" size="2" /> Dashboard
        </Link>
        <Link
          to="/admin/employee"
          className="list-group-item list-group-item-action bg-light"
        >
          <Icofont className="mr-1" icon="people" size="2" />
          Employee
        </Link>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="calendar" size="2" /> Holiday
        </a>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="data" size="2" /> Attendence
        </a>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="delicious" size="2" /> Depeartment
        </a>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="page" size="2" /> Leave
        </a>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="search-job" size="2" /> Recruitment
        </a>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="ui-calendar" size="2" />
          Calender
        </a>
        <a href="#" className="list-group-item list-group-item-action bg-light">
          <Icofont className="mr-1" icon="settings" size="2" /> Setting
        </a>
      </div>
    </div>
  );
}

export default AdminSidebar;
