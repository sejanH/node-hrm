import axios from "axios";
import jwt_decode from "jwt-decode";
import setAuthToken from "../utils/setAuthToken";

import { GET_ERRORS, SET_CURRENT_USER, CLEAR_ERRORS } from "./types";

export const loginUser = userData => dispatch => {
  axios
    .post("http://localhost:5000/api/auth/login", userData)
    .then(res => {
      //Save to Local storage
      const { token } = res.data;
      // Set token to local storage
      localStorage.setItem("jwt_token", token);
      // Set Token to Auth header
      setAuthToken(token);
      // Decode Token to get data
      const decoded = jwt_decode(token);
      //set current user
      dispatch(setCurrentUser(decoded));
      dispatch(clearError());
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Set Logged In User
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};
// Set Logged In User
export const clearError = () => {
  return {
    type: CLEAR_ERRORS,
    payload: {}
  };
};
