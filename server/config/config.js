require("dotenv").config();
module.exports = {
  development: {
    //"use_env_variable": "NODE_ENV",
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: "mysql",
    logging: false,
    // exclude createdAt,updatedAt field from all queries
    define: {
      "defaultScope": {
        "attributes": {
          "exclude": ["createdAt", "updatedAt"]
        }
      }
    }
  },
  test: {
    //"use_env_variable": "NODE_ENV",
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: "mysql",
    logging: false,
    // exclude createdAt,updatedAt field from all queries
    define: {
      "defaultScope": {
        "attributes": {
          "exclude": ["createdAt", "updatedAt"]
        }
      }
    }
  },
  production: {
    //"use_env_variable": "NODE_ENV",
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: "mysql",
    logging: false,
    // exclude createdAt,updatedAt field from all queries
    define: {
      "defaultScope": {
        "attributes": {
          "exclude": ["createdAt", "updatedAt"]
        }
      }
    }
  }
};
