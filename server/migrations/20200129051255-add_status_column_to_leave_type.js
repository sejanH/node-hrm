'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'leavetypes',
        'status',
        {
          type: Sequelize.INTEGER(1),
          defaultValue: 1,
          after: 'max_allowed'
        }
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('leavetypes', 'status')
    ]);
  }
};
