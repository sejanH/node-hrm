'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'leaves',
        'leavedates',
        {
          type: Sequelize.STRING,
          after: 'day_count'
        }
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('leaves', 'leavedates')
    ]);
  }
};
