'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'jobs',
        'job_responsibility',
        {
          type: Sequelize.TEXT,
          after: 'description'
        }
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('jobs', 'job_responsibility')
    ]);
  }
};
