"use strict";
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable("employeeinfos", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            EmployeeId: {
                type: Sequelize.INTEGER(11),
                allowNull: true,
                references: {
                    model: "employees",
                    key: "id"
                },
                onUpdate: "CASCADE",
                onDelete: "CASCADE"
            },
            first_name: {
                type: Sequelize.STRING(50)
            },
            last_name: {
                type: Sequelize.STRING(50)
            },
            present_address: {
                type: Sequelize.STRING(150),
                allowNull: true
            },
            permanent_address: {
                type: Sequelize.STRING(150),
                allowNull: true
            },
            dob: {
                type: Sequelize.DATE,
                allowNull: true
            },
            join_date: {
                type: Sequelize.DATE,
                allowNull: true
            },
            leave_date: {
                type: Sequelize.DATE,
                allowNull: true
            },
            probation_period: {
                type: Sequelize.STRING(10),
                allowNull: true
            },
            salary_scale: {
                type: Sequelize.STRING(10),
                allowNull: true
            },
            work_days: {
                type: Sequelize.INTEGER(1),
                allowNull: true
            },
            blood: {
                type: Sequelize.STRING(11),
                allowNull: true
            },
            designation: {
                type: Sequelize.STRING(60),
                allowNull: true
            },
            photo: {
                type: Sequelize.STRING(255),
                allowNull: true
            },
            phone: {
                type: Sequelize.STRING(20),
                allowNull: true
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)')
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3)')
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable("employeeinfos");
    }
};