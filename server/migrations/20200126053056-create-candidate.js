'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Candidates', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11)
      },
      JobId: {
        type: Sequelize.INTEGER(11),
        references: {
          model: "jobs",
          key: "id"
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE"
      },
      email: {
        type: Sequelize.STRING(255)
      },
      name: {
        type: Sequelize.STRING(150)
      },
      phone: {
        type: Sequelize.STRING(20)
      },
      cv_file: {
        type: Sequelize.STRING(255)
      },
      portfolio: {
        type: Sequelize.STRING(255)
      },
      hire: {
        type: Sequelize.INTEGER(1),
        defaultValue: 0
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)')
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3)')
      }
    }, {
      uniqueKeys: {
        job_candidate_unique: {
          fields: ['JobId', 'email']
        }
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Candidates');
  }
};