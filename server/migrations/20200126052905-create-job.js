'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Jobs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11)
      },
      DepartmentId: {
        type: Sequelize.INTEGER(11),
        allowNull: true,
        references: {
          model: "departments",
          key: "id"
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE"
      },
      description: {
        type: Sequelize.TEXT
      },
      tags: {
        type: Sequelize.TEXT
      },
      salary_range: {
        type: Sequelize.STRING
      },
      required_experience: {
        type: Sequelize.STRING
      },
      notes: {
        type: Sequelize.TEXT
      },
      facilities: {
        type: Sequelize.TEXT
      },
      deadline: {
        type: Sequelize.DATEONLY
      },
      status: {
        type: Sequelize.INTEGER(2),
        defaultValue: 1
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)')
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3)')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Jobs');
  }
};