'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'jobs',
        'slug',
        {
          type: Sequelize.UUID,
          after: 'DepartmentId'
        }
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('jobs', 'slug')
    ]);
  }
};
