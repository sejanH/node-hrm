'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'attendances',
        'attendance_date',
        {
          type: Sequelize.STRING(60),
          after: 'id'
        }
      )
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('attendances', 'attendance_date')
    ]);
  }
};
