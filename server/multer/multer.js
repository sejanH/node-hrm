const uuidv4 = require("uuid/v4");

// Multer
const multer = require("multer");
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "./multer/image");
    },
    filename: function (req, file, cb) {
        let fileName = uuidv4() + file.originalname;
        cb(null, fileName);
    }
});

// var upload = multer({ storage: storage }).single("image");

const fileFilter = function (req, file, cb) {
    if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
        cb(null, true);
    } else {
        cb({ msg: "File Type not supportad" }, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 2
    },
    fileFilter: fileFilter
}).single("photo");


const uploadFile = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "./multer/files");
        },
        filename: function (req, file, cb) {
            let fileName = uuidv4() + '__' + file.originalname;
            cb(null, fileName);
        }
    }),
    limits: {
        fileSize: 1024 * 1024 * 4
    },
    fileFilter: function (req, file, cb) {
        if (file.mimetype === "application/pdf") {
            cb(null, true);
        } else {
            cb({ msg: "File Type not supported" }, false);
        }
    }

}).single("cv_file");

const uploadAttendenceFile = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "./multer/attendenceXlFile");
        },
        filename: function (req, file, cb) {
            let fileName = uuidv4() + '__' + file.originalname;
            cb(null, fileName);
        }
    }),
    limits: {
        fileSize: 1024 * 1024 * 4
    },
    fileFilter: function (req, file, cb) {
        if (file.mimetype === "application/vnd.ms-excel" || file.mimetype === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            cb(null, true);
        } else {
            cb({ msg: "File Type not supported" }, false);
        }
    }

}).single("xlFile");


module.exports.upload = upload;
module.exports.uploadFile = uploadFile;
module.exports.uploadAttendenceFile = uploadAttendenceFile;