// db.js not used here try config/config.js
const Sequelize = require("sequelize");
/*
format: Sequelize(database,username,password){}
*/
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST,
    dialect: "mysql"
});
// sequelize.authenticate();

function setConn(db) {
    try {
        let sequelize_connection = db.authenticate();
        console.log('Connection has been established successfully.');
        return sequelize_connection;
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

module.exports = { sequelize, setConn };
global.sequelize = sequelize;