require('dotenv').config();
const jwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;

//model
const Model = require('../models/index');
const Employee = require('../models/employee');

const options = {};

options.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
options.secretOrKey = process.env.SECRET;

module.exports = passport => {
    passport.use(
        new jwtStrategy(options, (jwt_payload, done) => {
            Model.Employee.findByPk(jwt_payload.id,
                { attributes: { exclude: ['email', 'password'] } })
                .then(emp => {
                    if (emp && emp.role_status == jwt_payload.role) {
                        return done(null, emp);
                    }
                    return done(null, false);
                }).catch(err => {
                    console.log(err);
                })
        })
    );
};