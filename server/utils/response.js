const successMessage = (res, status, msg) => {
    return res.status(status).json({ msg });
}
const successMessageWithData = (res, status, msg, data) => {
    return res.status(status).json({ msg, data });
}
const errorMessage = (res, status, msg) => {
    return res.status(status).json({ msg });
}
const errorMessageWithData = (res, status, msg, err) => {
    return res.status(status).json({ msg, err });
}


module.exports.successMessage = successMessage
module.exports.successMessageWithData = successMessageWithData
module.exports.errorMessage = errorMessage
module.exports.errorMessageWithData = errorMessageWithData