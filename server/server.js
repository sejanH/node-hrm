require('dotenv').config();
// require('./utils/db').setConn(sequelize);
//modules
const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const passport = require('passport');

//middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
//passport config
require("./utils/passport")(passport);

//cors
let options = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Request-Methods": "GET,PUT,POST,DELETE,OPTIONS",
    "X-Requested-With": "XMLHttpRequest",
    "Access-Control-Allow-Headers": "Content-Type, Authorization, Content-Length, X-Requested-With"
};
app.use(cors(options));

//routes
const auth = require('./routes/api/Auth/auth');
const department = require('./routes/api/Admin/department');
const holiday = require('./routes/api/Admin/holiday');
const employee = require('./routes/api/Admin/employee');
const leaveType = require('./routes/api/Admin/leavetype');
const profile = require('./routes/api/profile');
const employeeLeave = require('./routes/api/Employee/leave');
const adminLeave = require('./routes/api/Admin/leave');
const jobs = require('./routes/api/Admin/jobs');
const candidates = require('./routes/api/Admin/candidate');
const activeJobs = require('./routes/api/active-jobs')
const attendance = require('./routes/api/Admin/attendence')

// console.log(auth)


app.use('/api/auth', auth); // login and registration
/* Admin Routes */
app.use('/api/admin/department', department); // update/add new department
app.use('/api/admin/holiday', holiday); // manage holidays
app.use('/api/admin/employee', employee); // manage Employee
app.use('/api/admin/leavetype', leaveType);//maintain types of leaves
app.use('/api/admin/jobs', jobs);//manage job circulars
app.use('/api/admin/candidates', candidates);
app.use('/api/admin/attendance', attendance);
/* Employee Routes */
app.use('/api/profile', profile); // profile update
app.use('/api/employee/leave', employeeLeave); // Manage Leave
app.use('/api/admin/leave', adminLeave); // Manage Leave
// app.use('/api/employee/leave', employeeLeave); // Manage Leave
app.use('/api/career', activeJobs);// get list of all active jobs





const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`server running at port ${PORT}`);
})