"use strict";
module.exports = (sequelize, DataTypes) => {
  const EmployeeInfo = sequelize.define(
    "EmployeeInfo",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      EmployeeId: {
        type: DataTypes.INTEGER(11),
        unique: true
      },
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      present_address: DataTypes.STRING,
      permanent_address: DataTypes.STRING,
      dob: DataTypes.DATE,
      join_date: DataTypes.DATE,
      leave_date: DataTypes.DATE,
      probation_period: DataTypes.STRING,
      salary_scale: DataTypes.STRING,
      work_days: DataTypes.INTEGER,
      blood: DataTypes.STRING,
      designation: DataTypes.STRING,
      photo: DataTypes.STRING,
      phone: DataTypes.STRING
    },
    {}
  );
  EmployeeInfo.associate = function (models) {
    // associations can be defined here
    EmployeeInfo.belongsTo(models.Employee);
  };
  return EmployeeInfo;
};
