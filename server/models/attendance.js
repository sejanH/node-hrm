'use strict';
module.exports = (sequelize, DataTypes) => {
  const Attendance = sequelize.define('Attendance', {
    attendance_date: DataTypes.STRING,
    date: DataTypes.STRING,
    EmployeeId: DataTypes.INTEGER,
    machine_id: DataTypes.STRING,
    in_time: DataTypes.STRING,
    out_time: DataTypes.STRING,
    remark: DataTypes.STRING
  }, {});
  Attendance.associate = function (models) {
    // associations can be defined here
    Attendance.belongsTo(models.Employee);
  };
  return Attendance;
};