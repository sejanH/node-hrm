'use strict';
module.exports = (sequelize, DataTypes) => {
  const FingerCount = sequelize.define('FingerCount', {
    user_id: DataTypes.INTEGER,
    emp_id: DataTypes.STRING,
    punch_time: DataTypes.DATE
  }, {});
  FingerCount.associate = function (models) {
    // associations can be defined here
    // FingerCount.belongsTo(models.Employee);
  };
  return FingerCount;
};