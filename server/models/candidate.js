'use strict';
module.exports = (sequelize, DataTypes) => {
  const Candidate = sequelize.define('Candidate', {
    JobId: DataTypes.INTEGER,
    email: DataTypes.STRING,
    name: DataTypes.STRING,
    phone: DataTypes.STRING,
    cv_file: DataTypes.STRING,
    portfolio: DataTypes.STRING,
    hire: DataTypes.INTEGER
  }, {});
  Candidate.associate = function (models) {
    // associations can be defined here
    Candidate.belongsToMany(models.Job, { through: 'Candidates' });
    Candidate.hasOne(models.CandidateDetails, { as: 'CandidateDetails', foreignKey: 'CandidateId' });
  };
  return Candidate;
};