'use strict';
module.exports = (sequelize, DataTypes) => {
    const JobCandidate = sequelize.define('JobCandidate', {
        job_id: DataTypes.INTEGER,
        candidate_id: DataTypes.INTEGER
    }, {});
    JobCandidate.associate = function(models) {
        // associations can be defined here
        JobCandidate.belongsTo(models.Job, { foreignKey: "id" });
        JobCandidate.belongsTo(models.Candidate, { foreignKey: "id" });
    };
    return JobCandidate;
};