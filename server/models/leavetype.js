'use strict';
module.exports = (sequelize, DataTypes) => {
  const LeaveType = sequelize.define('LeaveType', {
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    max_allowed: DataTypes.INTEGER,
    status: DataTypes.INTEGER
  }, {});
  LeaveType.associate = function (models) {
    // associations can be defined here
    LeaveType.hasMany(models.Leave);
  };
  return LeaveType;
};