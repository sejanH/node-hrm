'use strict';
module.exports = (sequelize, DataTypes) => {
  const Job = sequelize.define('Job', {
    DepartmentId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    slug: {
      type: DataTypes.UUID,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4
    },
    description: DataTypes.TEXT,
    job_responsibility: DataTypes.TEXT,
    tags: DataTypes.TEXT,
    salary_range: DataTypes.STRING,
    required_experience: DataTypes.STRING,
    notes: DataTypes.TEXT,
    facilities: DataTypes.TEXT,
    deadline: DataTypes.DATEONLY,
    status: DataTypes.INTEGER
  }, {});
  Job.associate = function (models) {
    // associations can be defined here
    Job.belongsTo(models.Department);
    Job.hasMany(models.Candidate, { as: 'Candidate', foreignKey: 'JobId' });
  };
  return Job;
};