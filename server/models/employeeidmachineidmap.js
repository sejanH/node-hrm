'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeIdMachineIdMap = sequelize.define('EmployeeIdMachineIdMap', {
    machine_id: DataTypes.STRING,
    emp_id: DataTypes.INTEGER
  }, {});
  EmployeeIdMachineIdMap.associate = function (models) {
    // associations can be defined here
    EmployeeIdMachineIdMap.belongsTo(models.Employee, { foreignKey: "emp_id" });
  };
  return EmployeeIdMachineIdMap;
};