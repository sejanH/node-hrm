'use strict';
module.exports = (sequelize, DataTypes) => {
  const AttendanceImport = sequelize.define('AttendanceImport', {
    attendance_date: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    file_name: DataTypes.STRING,
    createdAt: DataTypes.DATE
  }, {
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  AttendanceImport.associate = function (models) {
    // associations can be defined here
  };
  return AttendanceImport;
};