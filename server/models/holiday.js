'use strict';
module.exports = (sequelize, DataTypes) => {
  const Holiday = sequelize.define('Holiday', {
    title: DataTypes.STRING,
    status: DataTypes.INTEGER,
    description: DataTypes.STRING,
    date_from: DataTypes.DATE,
    date_to: DataTypes.DATE
  }, {});
  Holiday.associate = function(models) {
    // associations can be defined here
  };
  return Holiday;
};