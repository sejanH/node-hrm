'use strict';
module.exports = (sequelize, DataTypes) => {
  const CandidateDetails = sequelize.define('CandidateDetails', {
    CandidateId: DataTypes.INTEGER,
    contacted: DataTypes.INTEGER,
    schedule: DataTypes.DATE,
    attended: DataTypes.INTEGER,
    score: DataTypes.TEXT,
    short_listed: DataTypes.INTEGER,
    remarks: DataTypes.TEXT
  }, {});
  CandidateDetails.associate = function (models) {
    // associations can be defined here
    CandidateDetails.belongsTo(models.Candidate);
  };
  return CandidateDetails;
};