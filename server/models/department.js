"use strict";
module.exports = (sequelize, DataTypes) => {
    const Department = sequelize.define(
        "Department", {
        title: DataTypes.STRING,
        description: DataTypes.TEXT
    }, {}
    );
    // associations can be defined here
    Department.associate = models => {
        Department.hasMany(models.Job);
        Department.hasMany(models.Employee);
    };
    return Department;
};