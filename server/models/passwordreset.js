'use strict';
module.exports = (sequelize, DataTypes) => {
  const passwordReset = sequelize.define('passwordReset', {
    email: DataTypes.STRING,
    slug: DataTypes.STRING,
    inaAt: DataTypes.STRING,
    expAt: DataTypes.STRING,
  }, {});
  passwordReset.associate = function (models) {
    // associations can be defined here
  };
  return passwordReset;
};