"use strict";
module.exports = (sequelize, DataTypes) => {
    const Employee = sequelize.define(
        "Employee", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        emp_id: DataTypes.STRING,
        role_status: DataTypes.INTEGER, // 1=Admin,0=Employee
        status: DataTypes.INTEGER,
        DepartmentId: DataTypes.INTEGER
    }, {
    }
    );
    // associations can be defined here
    Employee.associate = models => {
        Employee.belongsTo(models.Department);
        Employee.hasOne(models.EmployeeInfo, { as: "employeeinfo", foreignKey: "id" });
        Employee.hasMany(models.Attendance);
        // Employee.hasMany(models.FingerCount);
        Employee.hasMany(models.Leave);
        Employee.hasOne(models.EmployeeIdMachineIdMap, { as: "attendance_id", foreignKey: "emp_id" });
        // Employee.belongsToMany(models.Attendance, { through: models.EmployeeIdMachineIdMap });
    };
    return Employee;
};