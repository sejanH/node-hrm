'use strict';
module.exports = (sequelize, DataTypes) => {
    const Leave = sequelize.define('Leave', {
        EmployeeId: DataTypes.INTEGER,
        LeaveTypeId: DataTypes.INTEGER,
        date_from: DataTypes.DATE,
        date_to: DataTypes.DATE,
        day_count: DataTypes.INTEGER,
        leavedates: DataTypes.STRING,
        reason: DataTypes.TEXT,
        status: DataTypes.INTEGER,
        response_date: DataTypes.DATE
    }, {});
    Leave.associate = function (models) {
        // associations can be defined here
        Leave.belongsTo(models.LeaveType);
        Leave.belongsTo(models.Employee);
    };
    return Leave;
};