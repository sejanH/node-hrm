'use strict';
const { check } = require('express-validator');

module.exports = [
    check('EmployeeId').not().isEmpty(),
    check('LeaveTypeId').not().isEmpty(),
    check('date_from').not().isEmpty(),
    check('date_from').isISO8601().toDate().withMessage("Invalid Date"),
    check('date_to').not().isEmpty(),
    check('date_to').isISO8601().toDate().withMessage("Invalid Date"),
    check('reason').not().isEmpty(),
];