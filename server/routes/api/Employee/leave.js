require('dotenv').config();
const express = require('express');
const { validationResult } = require('express-validator');
const router = express.Router();
const passport = require('passport');
const moment = require("moment");
/* Validator */
const LeaveValidator = require('./validator/leave-validator');
/* Response Wrapper */
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../../utils/response');
/* Model import */
const Model = require('../../../models/index');
/* Functions */
//get date Function
async function getDates(startDate, stopDate, userId) {

    var dateArray = []; // empty Array
    var currentDate = moment(startDate); // start date in moment object
    var stopDate = moment(stopDate); // end date in moment object
    let work_days = 0;
    await Model.EmployeeInfo.findOne({
        attributes: ['work_days'],
        where: {
            EmployeeId: userId
        }
    }).then(emp => {
        work_days = emp.work_days;
    });

    if (work_days == 5) {
        while (currentDate <= stopDate) {

            var d = moment(currentDate).format('dddd');
            //console.log(date);
            if (d != "Friday" && d != "Saturday") {
                let date_range = currentDate.toISOString().split('T')[0];
                var query = `SELECT * FROM holidays WHERE status = 1 and "${date_range}" between date_from AND date_to`;
                await sequelize.query(query).spread((holidays) => {
                    if (holidays.length == 0) {
                        dateArray.push(moment(date_range).format('YYYY-MM-DD'))
                    }
                })
            }
            currentDate = moment(currentDate).add(1, 'days');
        }
    }
    if (work_days == 6) {
        while (currentDate <= stopDate) {

            var d = moment(currentDate).format('dddd');
            //console.log(date);
            if (d != "Friday") {
                let date_range = currentDate.toISOString().split('T')[0];
                var query = `SELECT * FROM holidays WHERE status = 1 and "${date_range}" between date_from AND date_to`;
                await sequelize.query(query).spread((holidays) => {
                    if (holidays.length == 0) {
                        dateArray.push(moment(date_range).format('YYYY-MM-DD'))
                    }
                })
            }
            currentDate = moment(currentDate).add(1, 'days');
        }
    }
    return JSON.stringify(dateArray);
}
/***************************************
 @route POST api/employee/Leave
 @desc Create a Leave Request
 @access Employee
****************************************/
router.post('/', passport.authenticate("jwt", { session: false }), LeaveValidator, async (req, res) => {
    //check validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        errorMessageWithData(res, 422, 'failed', errors.array())
    } else {
        if (req.user.role_status == 0) {
            const { EmployeeId, LeaveTypeId, date_to, date_from, reason } = req.body
            const diffTime = new Date(date_to) - new Date(date_from);
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)) + 1;
            if (diffDays < 1) {
                errorMessage(res, 400, "Invalid Date Difference!")
            } else {
                const dates = await getDates(date_from, date_to, req.user.id);
                let day_count = JSON.parse(dates).length
                Model.Leave.create({
                    EmployeeId,
                    LeaveTypeId,
                    date_to,
                    date_from,
                    day_count,
                    status: 0,// 0= no response, 1 = accepted , 2 = rejected 
                    leavedates: dates,
                    reason
                }).then(leave => {
                    successMessageWithData(res, 200, "success", leave)
                })
            }
        }
    }
})

/***************************************
 @route Get api/employee/Leave
 @desc Get all leave for single Employee
 @access Employee
****************************************/

router.get('/', passport.authenticate("jwt", { session: false }), (req, res) => {
    Model.Leave.findAll({
        include: [
            {
                model: Model.LeaveType
            }
        ],
        order: [
            ['id', "DESC"]
        ],
        where: {
            EmployeeId: req.user.id
        }
    }).then(leaves => {
        successMessageWithData(res, 200, "success", leaves)
    })
})


/***************************************
 @route Get api/employee/Leave/:id
 @desc Get a leave for single Employee
 @access Employee
****************************************/

router.get('/:id', passport.authenticate("jwt", { session: false }), (req, res) => {
    Model.Leave.findOne(
        {
            include: [{
                model: Model.LeaveType
            }],
            where: {
                id: req.params.id,
                EmployeeId: req.user.id
            }
        }
    ).then(leave => {
        if (leave)
            successMessageWithData(res, 200, "success", leave)
        else
            errorMessage(res, 400, "Leave Not Found")
    })
})

module.exports = router