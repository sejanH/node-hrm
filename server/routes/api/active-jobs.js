const express = require('express');
const router = express.Router();

/* Model import */
const Model = require('../../models/index');
/* Validator and Response Message*/
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../utils/response');
//multer
const multer = require("multer");
const { uploadFile } = require("../../multer/multer");


/******************
 @route GET api/career/:slug
 @desc Get all job circular list/single circular
 @access open
*********************/
router.get('/:slug?', async (req, res) => {
    Model.Job.findAll({
        include: ['Department'],
        where: {
            slug: typeof req.params.slug == 'undefined' ? { [Model.Sequelize.Op.not]: null } : req.params.slug,
            status: 1
        },
        order: [
            ['id', 'DESC']
        ]
    }).then(data => {
        if (data.length > 0) {
            return successMessageWithData(res, 200, 'success', data);
        } else {
            return errorMessageWithData(res, 201, 'success', 'job circular is expired or not active');
        }
    }).catch(err => {
        console.log(err);
        return;
    });;
});

/******************
 @route POST api/career/
 @desc apply for job circular
 @access open
*********************/
router.post('/:slug', async (req, res) => {
    await uploadFile(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return errorMessageWithData(res, 400, 'failed', err);// file size not more than 4MB
        } else if (err) {
            return errorMessageWithData(res, 400, 'failed', err);
        } else {
            if (!req.file) {
                return errorMessage(res, 400, 'select a pdf file');
            } else {
                const { JobId, name, email, phone, portfolio } = req.body;
                if (!JobId) {
                    errorMessage(res, 422, 'job department is required');
                    //throw new Error('job department is required');
                }
                if (!name) {
                    errorMessage(res, 422, 'name is required');
                    //throw new Error('name is required');
                }
                if (!email) {
                    errorMessage(res, 422, 'email is required');
                    //throw new Error('email is required');
                }
                if (!phone) {
                    errorMessage(res, 422, 'phone is required');
                    //throw new Error('phone is required');
                }

                Model.Candidate.create({
                    JobId, name, email, phone, portfolio,
                    cv_file: req.file.filename
                }).then(async response => {
                    await Model.CandidateDetails.create({
                        CandidateId: response.id,
                        contacted: 0,
                        schedule: null,
                        attendend: 0,
                        score: null,
                    }).then(data => {
                        successMessageWithData(res, 201, 'success', response);
                    }).catch(err => {
                        errorMessageWithData(res, 400, 'failed', err);
                    });

                }).catch(err => {
                    if (err instanceof Model.Sequelize.Error) {
                        if (err.original.errno == 1062) {
                            return errorMessageWithData(res, 400, 'failed', `${email} is already applied for this job circular`)
                        }
                        return errorMessageWithData(res, 400, 'failed', err.json())
                    } else {
                        return errorMessageWithData(res, 400, 'error', err);
                    }
                });
            }
        }
    })

});

module.exports = router;