require('dotenv').config();

const fs = require('fs');
const path = require("path");
const express = require('express');

const router = express.Router();
const passport = require('passport');

const multer = require("multer");
const { uploadAttendenceFile } = require("../../../multer/multer");
/* Response Wrapper */
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../../utils/response');
const { check, validationResult } = require('express-validator');

/* Model import */
const Model = require('../../../models/index');
const excelToJson = require('convert-excel-to-json');

Date.prototype.addMinutes = function (minutes) {
    this.setMinutes(this.getMinutes() + minutes);
    return this;
};
let machine_ids = [];
/* [
    '1', '10', '101', '102', '103', '104',
    '106', '107', '108', '109', '11', '110',
    '111', '112', '113', '12', '13', '14',
    '16', '17', '20', '21', '26', '27',
    '28', '31', '32', '33', '34', '35',
    '37', '38', '39', '40', '41', '42',
    '43', '44', '6', '7', '8'
]; */
//custom function
function parseData(result, date) {
    let arr = [];
    let convert = {
        D: 1,
        E: 2,
        F: 3,
        G: 4,
        H: 5,
        I: 6,
        J: 7,
        K: 8,
        L: 9,
        M: 10,
        N: 11,
        O: 12,
        P: 13,
        Q: 14,
        R: 15,
        S: 16,
        T: 17,
        U: 18,
        V: 19,
        W: 20,
        X: 21,
        Y: 22,
        Z: 23,
        AA: 24,
        AB: 25,
        AC: 26,
        AD: 27,
        AE: 28,
        AF: 29,
        AG: 30,
        AH: 31
    }
    for (let j = 1; j < result.length; j += 2) {

        if (machine_ids.indexOf(result[j].A) == -1) {
            machine_ids.push(result[j].A);
        }
    }
    for (i = 1; i < result.length; i += 2) {
        arr.push({
            name: result[i + 1].A,
            emp_id: result[i].A,
            value:
                Object.keys(result[i]).map(rs => {
                    return {
                        date: convert[`${rs}`],
                        in_time: new Date(result[i][rs]).addMinutes(7).toLocaleString().split(', ')[1],
                        out_time: result[i + 1][rs] ? new Date(result[i + 1][rs]).addMinutes(7).toLocaleString().split(', ')[1] : null,
                        machine_id: result[i].A,
                        attendance_date: date
                    }
                })
        })
    }
    return arr;
}

async function handleSubmit(parseResult, callback) {
    let counter = { employee: 0, attendance: 0 };
    let tmpCounter = { employee: 0, attendance: 0 };

    for (let i = 0; i < parseResult.length; i++) {
        let tmpAttendance = [];
        for (let j = 0; j < parseResult[i].value.length; j++) {
            if (parseResult[i].value[j].date) {
                tmpAttendance.push(parseResult[i].value[j]);
                tmpCounter.attendance += 1;
            }
        }
        await Model.Attendance.bulkCreate(tmpAttendance)
            .then(data => {
                counter.attendance = tmpCounter.attendance;
                counter.employee += 1;
            })
            .catch(err => {
                console.log(err)
            });
    }
    await updateEmployeeId();
    callback(counter);
}
/* Add emp id to attendance table */
async function updateEmployeeId() {
    Model.EmployeeIdMachineIdMap.findAll({
        attributes: ['machine_id', 'emp_id'],
        where: {
            machine_id: { [Model.Sequelize.Op.in]: machine_ids }
        }
    }).then(data => {
        data.map(d => {
            Model.Attendance.update(
                { EmployeeId: d.emp_id }, {
                where: {
                    machine_id: d.machine_id,
                    EmployeeId: { [Model.Sequelize.Op.eq]: null }
                }
            }).catch(err => {
                console.log(err)
            })
        });
    });
}
/* delete unneccessary uploads */
async function deleteFiles(dbData) {
    let file_names = [];
    dbData.map((d) => {
        file_names.push(d.file_name);
    });
    const dir = path.join(__dirname, '/../../../multer/attendenceXlFile/');
    fs.readdir(dir, (err, files) => {
        if (err) {
            console.log(err);
            return;
        }
        files.map((file, index) => {
            let fn = file_names.filter(d => d == file);
            if (fn.length == 0) {
                fs.unlinkSync(`${dir}${files[index]}`, cb => {
                    console.log(cb);
                });
            }
        });
    });
    return;
}
/******************
 @route POST api/admin/attendance/
 @desc Upload monthly attendance
 @access admin
*********************/
router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    if (req.user.role_status == 1) {
        // await updateEmployeeId();
        uploadAttendenceFile(req, res, function (err) {
            if (err instanceof multer.MulterError) {
                return res.status(400).json({ error: err });
            } else if (err) {
                // An unknown error occurred when uploading.Like filterImage

                return res.status(400).json({ error: err.message });
            } else {
                if (!req.file) {
                    return res.status(400).json({ error: "Select An File" });
                }
                // file read
                const result = excelToJson({
                    sourceFile: path.join(__dirname, `/../../../multer/attendenceXlFile/${req.file.filename}`)
                });
                // parse file for db import
                const month = req.body.month > 9 ? parseInt(req.body.month) : "0" + parseInt(req.body.month);
                const date = req.body.year + "-" + month + "-01";
                let parseResult = parseData(result.Sheet1, date);
                // insert into db after parsing
                Model.AttendanceImport.create({
                    attendance_date: date,
                    file_name: req.file.filename
                }).then(response => {
                    if (response) {
                        handleSubmit(parseResult, (data) => {
                            successMessageWithData(res, 200, "success", data);
                        });
                    } else {
                        return console.log('Failed')
                    }
                }).catch(async err => {
                    const dbData = await Model.AttendanceImport.findAll().then(response => { return response });
                    await deleteFiles(dbData);
                    if (err.original.errno == 1062) {
                        errorMessageWithData(res, 400, 'error', `Duplicate entry for date ${date}`);
                    } else {
                        return console.log(err.original.sqlMessage);
                    }
                });
            }
        })
    }
    else {
        errorMessage(res, 400, "Unauthorized")
    }
});

/******************
 @route POST api/admin/attendance/add-map
 @desc Add employee ID mapping with Attendance device id
 @access admin
*********************/
router.post('/add-map', passport.authenticate('jwt', { session: false }), [
    check('machine_id').not().isEmpty().bail().withMessage('Required'),
    check('emp_id').not().isEmpty().withMessage('Required')
], async (req, res) => {
    if (req.user.role_status == 1) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return errorMessageWithData(res, 422, 'failed', errors.array())
        } else {
            const { machine_id, emp_id } = req.body;
            Model.EmployeeIdMachineIdMap.findOrCreate({
                where: {
                    [Model.Sequelize.Op.or]: [{ machine_id }, { emp_id }]
                },
                defaults: {
                    machine_id, emp_id
                }
            }).then(data => {
                if (data === true) {
                    successMessage(res, 201, 'success')
                } else {
                    errorMessage(res, 200, 'already exists')
                }
            }).catch(err => {
                if (err.parent.errno = 1452) {
                    return errorMessageWithData(res, 400, 'error', 'Employee id is invalid')
                }
                console.log(err)
                return errorMessage(res, 400, 'error')
            });
        }
    }
    else {
        errorMessage(res, 400, "Unauthorized")
    }
});
/******************
 @route PUT api/admin/attendance/update-map
 @desc UPDATE employee ID mapping with Attendance device id
 @access admin
*********************/
router.put('/update-map', passport.authenticate('jwt', { session: false }), [
    check('machine_id').not().isEmpty().bail().withMessage('Required'),
    check('emp_id').not().isEmpty().withMessage('Required')
], async (req, res) => {
    if (req.user.role_status == 1) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return errorMessageWithData(res, 422, 'failed', errors.array())
        } else {
            const { machine_id, emp_id } = req.body;
            const otherEmployee = await Model.EmployeeIdMachineIdMap.findOne({
                where: {
                    machine_id
                }
            }).then(employee => { return employee }).catch(err => { console.log(err) });
            if (otherEmployee == null) {
                Model.EmployeeIdMachineIdMap.update({
                    machine_id
                }, {
                    where: {
                        emp_id
                    }
                }
                ).then(data => {
                    if (data == 1) {
                        successMessage(res, 201, 'success')
                    } else {
                        errorMessage(res, 200, 'no update')
                    }
                }).catch(err => {
                    if (err.parent.errno = 1452) {
                        return errorMessageWithData(res, 400, 'error', 'Employee id is invalid')
                    }
                    console.log(err)
                    return errorMessage(res, 400, 'error')
                });
            } else {
                return errorMessage(res, 400, 'duplicate machine id');
            }
        }
    }
    else {
        errorMessage(res, 400, "Unauthorized")
    }
});
/******************
 @route GET api/admin/attendance/delete-files
 @desc delete failed import files
 @access admin
*********************/
router.get('/delete-files', passport.authenticate('jwt', { session: false }), async (req, res) => {
    if (req.user.role_status == 1) {
        const dbData = await Model.AttendanceImport.findAll().then(response => { return response });
        await deleteFiles(dbData);
        successMessage(res, 200, 'success');
    } else {
        errorMessage(res, 400, "Unauthorized")
    }
});

/******************
 @route GET api/admin/attendance/get/:id?
 @desc Get attendance
 @access admin
*********************/
router.get('/get/:id?/?', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { id } = req.params;
    let { year, month, date } = req.query;
    if ((typeof year == 'undefined') || (year == '')) {
        year = '____';
    }
    if ((typeof month == 'undefined') || (month == '')) {
        month = '__';
    }
    if ((typeof date == 'undefined') || (date == '')) {
        date = null;
    }
    Model.Attendance.findAll({
        where: {
            machine_id: (typeof id == 'undefined') ? { [Model.Sequelize.Op.not]: null } : id,
            attendance_date: {
                [Model.Sequelize.Op.like]: `%${year}-${month}-__%`
            },
            EmployeeId: { [Model.Sequelize.Op.not]: null },
            date: date == null ? { [Model.Sequelize.Op.not]: null } : date
        },
        attributes: {
            exclude: (typeof id == 'undefined') ? [] : ['machine_id']
        }
    }).then(data => {
        successMessageWithData(res, 200, 'success', data);
    }).catch(err => {
        errorMessageWithData(res, 500, 'error', err);
    });
});

module.exports = router;