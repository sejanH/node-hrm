const express = require('express');
const { validationResult } = require('express-validator');
const router = express.Router();
const passport = require('passport');

/* Model import */
const Model = require('../../../models/index');
/* Validator and Response Message*/
const jobsValidator = require('./validators/jobs-validator');
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../../utils/response');

/******************
 @route GET api/admin/jobs/
 @desc Get all job circular list/single circular
 @access admin
*********************/
router.get('/:slug?', passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.role_status == 1) {
        Model.Job.findAll({
            include: ['Department', 'Candidate'],
            where: {
                slug: typeof req.params.slug == 'undefined' ? { [Model.Sequelize.Op.not]: null } : req.params.slug
            },
            order: [
                ['id', 'DESC'], ['updatedAt', 'DESC']
            ]
        }).then(data => {
            return successMessageWithData(res, 200, 'success', data);
        }).catch(err => {
            console.log(err);
            return;
        });
    } else {
        return errorMessage(res, 401, 'Unauthorized');
    }
});
/******************
 @route PUT api/admin/jobs/
 @desc Get all job circular list/single circular
 @access admin
*********************/
router.put('/update-job/', passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.role_status == 1) {
        const { id, DepartmentId, description, job_responsibility, tags, salary_range, required_experience, notes, facilities, deadline } = req.body;
        Model.Job.findAll({
            where: {
                id
            }
        }).then(data => {
            Model.Job.update({
                DepartmentId, description, job_responsibility, tags, salary_range, required_experience, notes, facilities, deadline
            }, {
                where: {
                    id
                }
            }).then(updated => {
                return successMessageWithData(res, 200, 'success', data);

            }).catch(err => {
                return errorMessageWithData(res, 401, 'failed', err);
            });
        }).catch(err => {
            console.log(err);
        });
    } else {
        return errorMessage(res, 401, 'Unauthorized');
    }
});
/******************
 @route POST api/admin/jobs/create-job
 @desc add new job circular
 @access admin
*********************/
router.post('/create-job', jobsValidator, passport.authenticate("jwt", { session: false }), async (req, res) => {
    if (req.user.role_status == 1) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return errorMessageWithData(res, 422, 'validation failed', errors.array())
        } else {
            const { DepartmentId, description, job_responsibility, tags, salary_range, required_experience, notes, facilities, deadline } = req.body;
            Model.Job.create({
                DepartmentId, description, job_responsibility, tags, salary_range, required_experience, notes, facilities, deadline
            }).then(job => {
                if (job.id > 0) {
                    return successMessage(res, 201, 'New job circular added');
                }
            }).catch(err => {
                // console.log(err.parent)
                return errorMessageWithData(res, 419, 'failed', { errno: err.parent.errno, message: err.parent.errno == 1452 ? `Department with id:${DepartmentId} does not exists` : err.parent.sqlMessage })
            })
        }
    } else {
        return errorMessage(res, 401, 'Unauthorized');
    }
});
/******************
 @route PUT api/admin/jobs/create-job
 @desc toggle single job status [active,inactive]
 @access admin
*********************/
router.put('/toggle-status/:id?/:status?', passport.authenticate("jwt", { session: false }), async (req, res) => {
    if (req.user.role_status == 1) {
        const { id, status } = req.params;
        if (id && status) {
            Model.Job.update({
                status
            }, {
                where: {
                    id
                }
            }).then(data => {
                if (data != 0) {
                    return successMessage(res, 200, 'updated')
                } else {
                    return successMessage(res, 304)
                }
            });
        } else {
            return errorMessage(res, 422, 'invalid');
        }
    } else {
        return errorMessage(res, 401, 'Unauthorized');
    }
});


module.exports = router;