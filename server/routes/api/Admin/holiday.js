const express = require('express');
const { validationResult } = require('express-validator');
const router = express.Router();
const passport = require('passport');
const Sequelize = require("sequelize");

/* Model import */
const Model = require('../../../models/index');
/* Validator and Response Message*/
const holidayValidator = require('./validators/holiday-validator');
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../../utils/response');

/******************
 @route GET api/admin/holiday/
 @desc Get holiday list
 @access admin
*********************/
router.get('/', passport.authenticate("jwt", { session: false }), async (req, res) => {
    Model.Holiday.findAll({ where: { status: 1 } }).then(data => {
        const today = new Date().toISOString().split('T')[0];
        let expiredHoliday = [];
        data.map(value => {
            if ((new Date(value.date_from) > new Date(today) == false) && (new Date(value.date_to) < new Date(today) == true)) {
                expiredHoliday.push(value.id);
                value.status = 0;
            }
        })
        successMessageWithData(res, 200, 'success', data);
        Model.Holiday.update({
            status: 0
        }, {
            where: {
                id: { [Sequelize.Op.in]: expiredHoliday }
            }
        });
        return;
    });
});
/******************
 @route POST api/admin/add-holiday
 @desc add a holiday
 @access admin
*********************/
router.post('/add-holiday', passport.authenticate("jwt", { session: false }), holidayValidator, async (req, res) => {
    if (req.user.role_status == 1) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            errorMessageWithData(res, 422, 'failed', errors.array())
        } else {
            const { title, description, date_from, date_to } = req.body;
            Model.Holiday.create({
                title,
                description: description == null ? '' : description,
                date_from: new Date(date_from),
                date_to: new Date(date_to)
            }).then(data => {
                if (data.id > 0) {
                    successMessage(res, 201, 'Holiday added');
                } else {
                    errorMessage(res, 203, 'failed');
                }
            }).catch(err => {
                errorMessageWithData(res, 500, 'failed', err);
            });
        }
    } else {
        errorMessage(res, 401, 'Unauthorized');
    }
});


/******************
 @route POST api/admin/update-holiday
 @desc update a holiday
 @access admin
*********************/
router.put('/update-holiday', passport.authenticate("jwt", { session: false }), holidayValidator, async (req, res) => {
    if (req.user.role_status == 1) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        } else {
            const { id, title, description, date_from, date_to } = req.body;
            Model.Holiday.findByPk(id, { attributes: { exclude: ['createdAt', 'updatedAt'] } }).then(data => {
                Model.Holiday.update({
                    title,
                    description: description == null ? '' : description,
                    date_from,
                    date_to
                }, {
                    where: {
                        id
                    }
                }).then(updatedHoliday => {
                    successMessage(res, 200, 'Holiday updated')
                })
            });
        }
    } else {
        errorMessage(res, 401, 'Unauthorized');
    }
});

/******************
 @route PUT api/admin/holiday/toggle-holiday
 @desc change holiday status
 @access admin
*********************/
router.put('/toggle-status', passport.authenticate("jwt", { session: false }), async (req, res) => {
    if (req.user.role_status == 1) {
        const { id, status } = req.body;
        Model.Holiday.update({
            status: status == 1 ? 0 : 1
        }, {
            where: {
                id
            }
        }).then(data => {
            successMessage(res, 200, 'success');
        })
    } else {
        errorMessage(res, 401, 'Unauthorized');
    }
});

/******************
 @route DELETE api/admin/delete-holiday
 @desc delete a holiday
 @access admin
*********************/
router.delete('/delete-holiday/:id', passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.role_status == 1) {
        const id = req.params.id;
        Model.Holiday.findByPk(id).then(response => {
            if (response) {
                Model.Holiday.destroy({
                    where: {
                        id
                    }
                }).then(msg => {
                    successMessage(res, 200, 'Holiday deleted');
                }).catch(err => {
                    errorMessageWithData(res, 500, 'Internal error', err)
                })
            } else {
                errorMessage(res, 404, 'Holiday not found');
            }
        })
    } else {
        errorMessage(res, 401, 'Unauthorized');
    }
});


module.exports = router;