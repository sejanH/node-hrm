'use strict';
const { check } = require('express-validator');

module.exports = [
    check('title').not().isEmpty(),
    check('date_from').not().isEmpty(),
    check('date_to').not().isEmpty()
];