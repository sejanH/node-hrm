'use strict';
const { check } = require('express-validator');

module.exports = [
    check('DepartmentId').not().isEmpty().trim().isLength({ min: 1 }).withMessage("Departmnet can't be empty"),
    check('description').not().isEmpty().trim(),
    check('salary_range').not().isEmpty().trim(),
    check('deadline').not().isEmpty().trim()
];