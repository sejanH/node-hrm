require('dotenv').config();
const express = require('express');

const router = express.Router();
const passport = require('passport');
/* Response Wrapper */
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../../utils/response');

/* Model import */
const Model = require('../../../models/index');

/******************
 @route GET api/admin/employee
 @desc Get All the Employee List
 @access admin
*********************/
router.get('/', passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.role_status == 1) {
        Model.Employee.findAll({
            attributes: { exclude: ['password'] },
            include: [{
                model: Model.EmployeeInfo,
                as: 'employeeinfo',
                attributes: {
                    exclude: ['id', 'EmployeeId']
                }
            }, {
                model: Model.EmployeeIdMachineIdMap,
                as: 'attendance_id'
            }],
            where: {
                role_status: 0
            }
        }).then(employees => {
            successMessageWithData(res, 200, "Success", employees)
        });
    } else {
        errorMessage(res, 401, 'Unauthorized');
    }
})

/******************
 @route GET api/admin/employee/{id}
 @desc Get get an employee
 @access admin
*********************/

router.get('/:id', passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.role_status == 1) {
        Model.Employee.findOne({
            attributes: { exclude: ['password'] },
            where: {
                id: req.params.id,
                role_status: 0
            }
        }).then(emp => {
            if (emp) successMessageWithData(res, 200, "Success", emp)
            else errorMessage(res, 404, "No data Found")
        })
    } else {
        errorMessage(res, 401, 'Unauthorized');
    }

})


module.exports = router;