const express = require('express');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const passport = require('passport');
const Sequelize = require("sequelize");

/* Model import */
const Model = require('../../../models/index');
/* Validator and Response*/
const holidayValidator = require('./validators/holiday-validator');
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../../utils/response');

/******************
 @route GET api/admin/leavetype/
 @desc Get Leave Types
 @access admin
*********************/
router.get('/', passport.authenticate("jwt", { session: false }),
    (req, res) => {
        if (req.user.role_status == 1) {
            Model.LeaveType.findAll({
                where: {
                    status: 1
                }
            }).then(data => {
                successMessageWithData(res, 200, 'success', data)
            })
        } else {
            errorMessage(res, 401, 'Unauthorized');
        }
    }
);

/******************
 @route POST api/admin/leavetype/
 @desc CREATE Leave Types
 @access admin
*********************/
router.post('/add-leavetype', passport.authenticate("jwt", { session: false }), [
    check('title').not().isEmpty()
],
    async (req, res) => {
        if (req.user.role_status == 1) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                errorMessageWithData(res, 422, 'failed', errors.array())
            }
            const { title, description, max_allowed } = req.body;
            Model.LeaveType.findOne({ where: { title: title.toLowerCase() } }).then(leavetype => {
                if (leavetype == null) {
                    Model.LeaveType.create({
                        title: title.toLowerCase(),
                        description: description == null ? '' : description,
                        max_allowed
                    }).then(data => {
                        if (data.id > 0) {
                            successMessage(res, 200, 'success');
                        } else {
                            errorMessage(res, 500, 'internal server error');
                        }
                    });
                } else {
                    errorMessage(res, 409, `Leave type ${title} exists`)
                }
            });
        } else {
            errorMessage(res, 401, 'Unauthorized');
        }
    }
);

/******************
 @route PUT api/admin/leavetype/update-leavetype
 @desc Update Leave Types
 @access admin
*********************/
router.put('/update-leavetype', passport.authenticate("jwt", { session: false }), [
    check('title').not().isEmpty()
],
    async (req, res) => {
        if (req.user.role_status == 1) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                errorMessageWithData(res, 422, 'failed', errors.array())
            } else {
                const { id, title, description, max_allowed } = req.body;
                Model.LeaveType.findAll({
                    where: {
                        title: title.toLowerCase(),
                        id: {
                            [Sequelize.Op.not]: id
                        }
                    }
                }).then(leavetype => {
                    Model.LeaveType.count({
                        where: {
                            id
                        }
                    }).then(data => {
                        idExists = data;
                        if (data == 0) {
                            errorMessage(res, 404, 'LeaveType id not found');
                        } else {
                            if (leavetype.length == 0) {
                                Model.LeaveType.update({
                                    title: title.toLowerCase(),
                                    description: description == null ? '' : description,
                                    max_allowed: max_allowed == null ? '' : max_allowed
                                }, {
                                    where: {
                                        id
                                    }
                                }).then(response => {
                                    successMessage(res, 201, "Leave Type Updated Successfully!");
                                });
                            } else {
                                errorMessage(res, 409, `Leave Type ${title} exists!!`);
                            }
                        }
                    });
                })

            }
        } else {
            errorMessage(res, 401, 'Unauthorized');
        }
    });


/******************
 @route DELETE api/admin/leavetype/delete-leavetype
 @desc Delete Leave Types
 @access admin
*********************/
router.delete('/delete-leavetype/:id', async (req, res) => {
    await Model.LeaveType.findByPk(req.params.id)
        .then(data => {
            if (data) {
                Model.LeaveType.update({
                    status: 0
                }, {
                    where: {
                        id: data.id
                    }
                }).then(success => {
                    successMessage(res, 201, 'deleted');
                })
            } else {
                errorMessage(res, 404, 'not found');
            }
        })
});

/******************
 @route PUT api/admin/leavetype/undo-delete
 @desc undo Delete Leave Type
 @access admin
*********************/
router.put('/undo-delete/:id', async (req, res) => {
    await Model.LeaveType.findOne({
        where: {
            id: req.params.id,
            status: 0
        }
    }).then(data => {
        if (data) {
            Model.LeaveType.update({
                status: 1
            }, {
                where: {
                    id: data.id
                }
            }).then(success => {
                successMessage(res, 201, 'restored');
            })
        } else {
            errorMessage(res, 404, 'not found');
        }
    })
});

module.exports = router;