require('dotenv').config();
const express = require('express');

const router = express.Router();
const passport = require('passport');
const Sequelize = require("sequelize");
/* Response Wrapper */
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../../utils/response');

/* Model import */
const Model = require('../../../models/index');

/******************
 @route GET api/admin/leave/
 @desc See All the Leave 
 @access admin
*********************/
//middleware to check blacklisted auth token
// router.use((req, res, next) => {
//     console.log(req.headers.authorization);
//     next();
// })


router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    if (req.user.role_status == 1) {

        Model.Leave.findAll({
            include: [
                {
                    model: Model.LeaveType
                }
            ],
            order: [
                ['id', "DESC"]
            ]
        }).then(leaves => {
            successMessageWithData(res, 200, "success", leaves)
        }).catch()

    } else {
        errorMessage(res, 404, "UnAuthorized")
    }

})

/******************
 @route GET api/admin/leave/:userId
 @desc See All the Leave for an employee
 @access admin
*********************/
router.get('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    if (req.user.role_status == 1) {

        Model.Leave.findAll({
            include: [
                {
                    model: Model.LeaveType
                }
            ],
            order: [
                ['id', "DESC"]
            ],
            where: {
                EmployeeId: req.params.id
            }
        }).then(leaves => {
            successMessageWithData(res, 200, "success", leaves)
        }).catch()

    } else {
        errorMessage(res, 404, "UnAuthorized")
    }

})

/******************
 @route GET api/admin/leave/show/:id
 @desc See a single leave by id
 @access admin
*********************/
router.get('/show/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    if (req.user.role_status == 1) {

        Model.Leave.findOne({
            include: [
                {
                    model: Model.LeaveType
                }
            ],
            where: {
                id: req.params.id
            }
        }).then(leaves => {
            if (leaves)
                successMessageWithData(res, 200, "success", leaves)
            else
                errorMessage(res, 400, "No data found")
        }).catch()

    } else {
        errorMessage(res, 404, "UnAuthorized")
    }

})


/******************
 @route GET api/admin/leave/update/:id
 @desc (Accept) update a single leave by id 
 @access admin
*********************/
router.put('/update/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    if (req.user.role_status == 1) {

        Model.Leave.findOne({
            include: [
                {
                    model: Model.LeaveType
                }
            ],
            where: {
                id: req.params.id
            }
        }).then(leave => {
            leave.update({
                status: 1
            }).then(leave => {
                successMessageWithData(res, 200, "updated", leave)
            })
        }).catch()

    } else {
        errorMessage(res, 404, "UnAuthorized")
    }

})

/******************
 @route GET api/admin/leave/reject/:id
 @desc (reject) update a single leave by id 
 @access admin
*********************/
router.put('/reject/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    //console.log(req.headers.authorization   );
    if (req.user.role_status == 1) {

        Model.Leave.findOne({
            include: [
                {
                    model: Model.LeaveType
                }
            ],
            where: {
                id: req.params.id
            }
        }).then(leave => {
            if (leave.status == 0) {
                leave.update({
                    status: 2
                }).then(leave => {
                    successMessageWithData(res, 200, "rejected", leave)
                })
            } else {
                errorMessage(res, 400, "This Leave request alreay accepted Or Rejected!")
            }

        }).catch()

    } else {
        errorMessage(res, 404, "UnAuthorized")
    }

})


module.exports = router;