require('dotenv').config();
const express = require('express');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const passport = require('passport');
const Sequelize = require("sequelize");

/* Model import */
const Model = require('../../../models/index');
// const Department = require('../../../models/department');

const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../../utils/response');

/******************
 @route POST api/admin/add-department
 @desc add a department
 @access admin
*********************/
router.post('/add-department', passport.authenticate("jwt", { session: false }), [
    check('title').not().isEmpty().withMessage('Department title is required'),
], async (req, res) => {
    if (req.user.role_status == 1) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        } else {
            const { title, description } = req.body;
            Model.Department.findOne({
                where: {
                    title: title.toLowerCase()
                }
            }).then(dept => {
                if (dept == null) {
                    Model.Department.create({
                        title: title.toLowerCase(),
                        description: description
                    }).then(response => {
                        return res.status(201).json({ msg: "Department Created Successfully!" })
                    });
                } else {
                    return res.status(409).json({ error: "Department with same title exists!!" });
                }
            });
        }
    } else {
        return res.status(401).json({ error: "Unauthorized" });
    }
});


/*****************************************
 @route PUT api/admin/update-department  
 @desc update department 
 @access admin
******************************************/
router.put('/update-department', passport.authenticate("jwt", { session: false }), [
    check('title').not().isEmpty().withMessage('Department title is required'),
], async (req, res) => {
    if (req.user.role_status == 1) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        } else {
            const { id, title, description } = req.body;
            Model.Department.findAll({
                where: {
                    title: title.toLowerCase(),
                    id: {
                        [Sequelize.Op.not]: id
                    }
                }
            }).then(dept => {
                Model.Department.count({
                    where: {
                        id
                    }
                }).then(data => {
                    if (data == 0) {
                        errorMessage(res, 404, 'Department not found');
                    }
                    else {
                        if (dept.length == 0) {
                            Model.Department.update({
                                title: title.toLowerCase(),
                                description: description == null ? '' : description
                            }, {
                                where: {
                                    id
                                }
                            }).then(response => {
                                successMessage(res, 201, "Department Updated Successfully!");
                            });
                        } else {
                            errorMessage(res, 409, `Department ${title} exists!!`)
                        }
                    }
                });
            });
        }
    } else {
        errorMessage(res, 401, 'Unauthorized');
    }
});

/******************
 @route GET api/admin/depeartments
 @desc get department
 @access admin
*********************/
router.get('/', passport.authenticate("jwt", { session: false }), (req, res) => {
    if (req.user.role_status == 1) {
        Model.Department.findAll().then(depeartments => {
            if (depeartments) {
                successMessageWithData(res, 200, "success", depeartments)
            } else {
                successMessage(res, 204, "No Depeartment Found!")
            }
        }).catch(err => {
            errorMessage(res, status, err)
        })
    } else {
        errorMessage(res, 404, "Unauthorized")
    }

})

module.exports = router;