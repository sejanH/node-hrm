const express = require('express');
const { validationResult } = require('express-validator');
const router = express.Router();
const passport = require('passport');

/* Model import */
const Model = require('../../../models/index');
/* Validator and Response Message*/
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../../utils/response');

/******************
 @route GET api/admin/candidate/
 @desc Get all job circular candidate list
 @access admin
*********************/
router.get('/:slug?', passport.authenticate("jwt", { session: false }), async (req, res) => {
    if (req.user.role_status == 1) {
        const slug = req.params.slug;
        if (!slug) {
            return res.status(404).send();
        }
        Model.Job.findOne({
            include: [{ model: Model.Candidate, as: "Candidate", include: [{ model: Model.CandidateDetails, as: 'CandidateDetails' }] }],
            where: {
                slug,
                status: 1
            }
        }).then(data => {
            successMessageWithData(res, 200, 'success', data);
        })

        // Model.Job.findOne({
        //     where: {
        //         slug,
        //         status: 1
        //     }
        // }).then(data => {
        //     if (data) {
        //         Model.Candidate.findAll({
        //             include: ['CandidateDetails'],
        //             where: {
        //                 JobId: data.id
        //             }
        //         }).then(response => {
        //             return successMessageWithData(res, 200, 'success', response);
        //         });
        //     } else {
        //         return errorMessage(res, 200, 'not active job');
        //     }
        // }).catch(err => {
        //     console.log(err);
        //     errorMessageWithData(res, 400, 'error', err);
        // })
    } else {
        return errorMessage(res, 401, 'Unauthorized');
    }
});
/******************
 @route GET api/admin/candidate/job_slug/candidate_id
 @desc Get single candidate
 @access admin
*********************/
router.get('/:slug/:candidate_id', passport.authenticate("jwt", { session: false }), async (req, res) => {
    if (req.user.role_status == 1) {
        const slug = req.params.slug;
        const candidate_id = req.params.candidate_id;
        if (!slug) {
            return res.status(404).send();
        }
        Model.Job.findOne({
            include: [{ model: Model.Candidate, as: "Candidate", include: [{ model: Model.CandidateDetails, as: 'CandidateDetails' }], where: { id: candidate_id } }],
            where: {
                slug,
                status: 1
            }
        }).then(data => {
            return successMessageWithData(res, 200, 'success', data);
        }).catch(err => {
            return errorMessageWithData(res, 400, 'error', err);
        })
    } else {
        return errorMessage(res, 401, 'Unauthorized');
    }
});
/******************
 @route PUT api/admin/candidates/hire
 @desc Update candiate details
 @access admin
*********************/
router.put('/hire/candidate', passport.authenticate("jwt", { session: false }), async (req, res) => {
    if (req.user.role_status == 1) {
        const { id, JobId } = req.body;
        console.log(req.params);
        Model.Job.findOne({
            include: [{ model: Model.Candidate, as: "Candidate", where: { id, JobId } }],
            where: {
                id: JobId, status: 1
            }
        }).then(data => {
            if (data) {
                Model.Candidate.update({
                    hire: 1
                }, {
                    where: {
                        id
                    }
                }).then(response => {
                    return successMessageWithData(res, 200, 'success', response);
                }).catch(err => {
                    return errorMessageWithData(res, 400, 'error', err);
                });

            } else {
                return errorMessage(res, 400, 'candidate and job mismatch')
            }
        }).catch(err => {
            return errorMessageWithData(res, 400, 'error', err);
        });
    } else {
        return errorMessage(res, 401, 'Unauthorized');
    }
});

/******************
 @route PUT api/admin/candidate/job_slug/candidate_id
 @desc Update candiate details
 @access admin
*********************/
router.put('/:slug', passport.authenticate("jwt", { session: false }), async (req, res) => {
    if (req.user.role_status == 1) {
        const { slug } = req.params;
        if (!slug) {
            return res.status(404).send();
        }
        const { CandidateId, contacted, schedule, attended, score, short_listed, remarks } = req.body;
        // console.log(req.body);
        Model.Job.findOne({
            include: [{ model: Model.Candidate, as: "Candidate", where: { id: CandidateId } }],
            where: {
                slug, status: 1
            }
        }).then(data => {
            if (data != null) {
                Model.CandidateDetails.update({
                    contacted, schedule, attended, score, short_listed, remarks
                },
                    {
                        where: {
                            CandidateId
                        }
                    }).then(data => {
                        return successMessageWithData(res, 200, 'success', data);
                    }).catch(err => {
                        return errorMessageWithData(res, 400, 'error', err);
                    });
            } else {
                return errorMessage(res, 400, 'candidate and job mismatch')
            }
        }).catch(err => {
            return errorMessageWithData(res, 400, 'error', err);
        });
    } else {
        return errorMessage(res, 401, 'Unauthorized');
    }
});

module.exports = router;