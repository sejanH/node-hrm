require('dotenv').config();
const express = require('express');

const passport = require('passport');

const router = express.Router();
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../utils/response');


/* Model import */
const Model = require('../../models/index');
// // Multer
const multer = require("multer");
const { upload } = require("../../multer/multer");

//validation
const registerEmployeeValidation = require('./Auth/validators/register-employee-validator');
const { validationResult } = require('express-validator');

/******************
 @route POST api/profile/upload-image
 @desc Upload an Image 
 @access auth
*********************/

router.post('/upload-image', passport.authenticate("jwt", { session: false }), (req, res) => {
    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(400).json({ error: err.message });
        } else if (err) {
            // An unknown error occurred when uploading.Like filterImage
            return res.status(400).json({ error: err.message });
        } else {
            if (!req.file) {
                return res.status(400).json({ error: "Select An Image" });
            }
            Model.EmployeeInfo.findOne({
                where: {
                    EmployeeId: req.user.id
                }
            }).then(empInfo => {

                empInfo.update({ photo: req.file.filename }

                )
                    .then(result => {
                        return res.status(202).json({ msg: "Image Upload Successfull!" })
                    }

                    )
                    .catch(err => { console.log(err) })
            }).catch()
        }
    })
})


/******************
 @route POST api/profile/update-employee-profile
 @desc Update employee profile 
 @access admin
*********************/

router.post("/update-employee-profile", passport.authenticate("jwt", { session: false }), async (req, res) => {

    /** Admin can update
    1. salary_scale
    2. designation
    3. probation_period
    4. work_days
    5. leave_date
    6. first_name
    7. last_name
    8. present_address
    **/

    // EmployeeID field is must, role should be admin
    if (req.user.role_status == 1 && req.body.EmployeeId) {

        Model.Employee.findOne({
            where: {
                id: req.body.EmployeeId
            }
        }).then(response => {
            //if employee found
            if (response) {
                let empInfo = {};
                const { salary_scale, designation, probation_period, work_days, leave_date, first_name, last_name, present_address } = req.body;
                if (salary_scale) empInfo.salary_scale = salary_scale;
                if (designation) empInfo.designation = designation;
                if (probation_period) empInfo.probation_period = probation_period;
                if (work_days) empInfo.work_days = work_days;
                if (leave_date) empInfo.leave_date = leave_date;
                if (first_name) empInfo.first_name = first_name;
                if (last_name) empInfo.last_name = last_name;
                if (present_address) empInfo.present_address = present_address;

                Model.EmployeeInfo.update(
                    { ...empInfo },
                    {
                        where:
                            { EmployeeId: req.body.EmployeeId }
                    }
                ).then(emp => {
                    successMessageWithData(res, 200, "success", emp)
                }).catch(err => {
                    errorMessageWithData(res, 400, "error", err)
                })
            } else {
                errorMessage(res, 400, "Employee Not Found!!")
            }
        })
    }
    else {
        errorMessage(res, 400, "Cann't update the employee info!!")
    }
});

module.exports = router;


/******************
 @route GET api/profile/deactivate a Employee
 @desc Deactivate a employee
 @access admin
*********************/

router.get('/deactivate-employee/:empId', passport.authenticate('jwt', { session: false }), (req, res) => {

    // Only admin can deactivate a user
    if (req.user.role_status == 1) {
        Model.Employee.findOne({ where: { id: req.params.empId } }).then(emp => {
            if (emp) {
                emp.update({ status: 0 }).then(response => {
                    if (response) {
                        Model.EmployeeInfo.update(
                            {
                                leave_date: Date.now()
                            },
                            { where: { EmployeeId: req.params.empId } }
                        ).then(success => {
                            if (success) successMessage(res, 200, "Employee Deactivate Successfully...!!")
                        })
                    }
                })
            } else {
                errorMessage(res, 400, "This User Is Not exists")
            }
        })
    }
    else {
        errorMessage(res, 401, "Unauthorized")
    }
})



/******************
 @route POST api/profile/update-employee-profile
 @desc Update employee profile 
 @access Employee
*********************/

router.post("/update-profile", passport.authenticate('jwt', { session: false }), (req, res) => {

    /** Employee can update
    1. first_name
    2. last_name
    3. dob (YYYY-MM-DD)
    4. present_address
    5. permanent_address
    6. blood
    7. phone
    **/
    let empInfo = {};
    const { first_name, last_name, dob, present_address, permanent_address, blood, phone } = req.body;
    if (dob) empInfo.dob = dob;
    if (permanent_address) empInfo.permanent_address = permanent_address;
    if (blood) empInfo.blood = blood;
    if (phone) empInfo.phone = phone;
    if (first_name) empInfo.first_name = first_name;
    if (last_name) empInfo.last_name = last_name;
    if (present_address) empInfo.present_address = present_address;

    if (req.user.role_status == 0) {
        Model.EmployeeInfo.update(
            {
                ...empInfo
            },
            { where: { EmployeeId: req.user.id } }
        ).then(response => {
            if (response) {
                successMessage(res, 200, "Profile Updated Successfully..!")
            }
            else {
                errorMessage(res, 400, "Something wrong ...!")
            }
        }).catch()
    }
    else {
        errorMessage(res, 400, "Unauthorized")
    }
})

/******************
 @route POST api/profile/update-image
 @desc Update employee image 
 @access Employee
*********************/

router.post("/update-image", passport.authenticate('jwt', { session: false }), (req, res) => {

    if (req.user.role_status == 0) {
        upload(req, res, function (err) {
            if (err instanceof multer.MulterError) {
                return res.status(400).json({ error: err.message });
            } else if (err) {
                // An unknown error occurred when uploading.Like filterImage
                return res.status(400).json({ error: err.message });
            } else {
                if (!req.file) {
                    return res.status(400).json({ error: "Select An Image" });
                }
                Model.EmployeeInfo.findOne({
                    where: {
                        EmployeeId: req.user.id
                    }
                }).then(empInfo => {

                    empInfo.update({ photo: req.file.filename }

                    )
                        .then(result => {
                            return res.status(202).json({ msg: "Image Upload Successfull!" })
                        }

                        )
                        .catch(err => { console.log(err) })
                }).catch()
            }
        })
    }
    else {
        errorMessage(res, 401, "Unauthorized")
    }
})




