require("dotenv").config();
const express = require("express");
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");

const {
  successMessage,
  successMessageWithData,
  errorMessage,
  errorMessageWithData
} = require("../../../utils/response");
const router = express.Router();

/* Model import */
const Model = require("../../../models/index");
const Employee = require("../../../models/employee");
const EmployeeInfo = require("../../../models/employeeinfo");

/******************
 @route POST api/auth/login
 @desc login user
 @access public
*********************/
router.post(
  "/login",
  [
    check("email")
      .not()
      .isEmpty()
      .isEmail()
      .withMessage("Email is not valid"),
    check("password")
      .not()
      .isEmpty()
      .withMessage("Password is required")
      .isLength({ min: 6 })
      .withMessage("Password Minimum length is 6")
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array()[0].msg });
    }
    const { email, password } = req.body;
    Model.Employee.findOne({
      include: ["employeeinfo"],
      where: {
        email
      }
    }).then(employee => {
      //if employee email found
      if (employee) {
        bcrypt.compare(password, employee.dataValues.password).then(matched => {
          //if password matched
          if (matched) {
            //create jwt payload
            const payload = {
              id: employee.id,
              role: employee.role_status,
              name:
                employee.employeeinfo.first_name +
                " " +
                employee.employeeinfo.last_name
            };
            //sign token
            jwt.sign(
              payload,
              process.env.SECRET,
              { expiresIn: "1d" },
              (err, token) => {
                res.json({ success: true, token: "Bearer " + token });
              }
            );
          } else {
            return res
              .status(400)
              .json({ error: "Your Provided Credential is not Correct!" });
          }
        });
      } else {
        return res
          .status(400)
          .json({ error: "Your Provided Credential is not Correct!" });
      }
    });
  }
);

/******************
 @route POST api/auth/change-passowrd
 @desc Change password
 @access employee/Admin
*********************/

router.post(
  "/change-password",
  passport.authenticate("jwt", { session: false }),
  [
    check("oldPassword")
      .not()
      .isEmpty(),
    check("newPassword")
      .not()
      .isEmpty(),
    check("confirmNewPassword")
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const { oldPassword, newPassword, confirmNewPassword } = req.body;

    const user = await Model.Employee.findOne({
      where: { id: req.user.id }
    }).then(user => {
      if (user) {
        return user;
      } else {
        return {};
      }
    });

    if (newPassword == confirmNewPassword) {
      if (Object.keys(user).length > 0) {
        bcrypt.compare(oldPassword, user.password).then(matched => {
          if (matched) {
            bcrypt.genSalt(parseInt(process.env.SALT) || 10, (err, salt) => {
              bcrypt.hash(newPassword, salt, (err, hash) => {
                if (err) throw err;
                else {
                  Model.Employee.update(
                    { password: hash },
                    {
                      where: {
                        id: req.user.id
                      }
                    }
                  ).then(response => {
                    successMessage(res, 200, "Password Changed Successfully");
                  });
                }
              });
            });
          } else {
            errorMessage(res, 400, "Old Password doesn't match");
          }
        });
      } else {
        errorMessage(res, 400, "User Not Found");
      }
    } else {
      errorMessage(
        res,
        400,
        " new Password and ConfirmNew password doesn't match "
      );
    }
  }
);

router.get(
  "/test",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    return res
      .status(200)
      .json({ id: req.user.id, role: req.user.role_status });
  }
);

module.exports.login = router;
