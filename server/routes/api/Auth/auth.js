const express = require('express');

const router = express.Router();

const { register } = require('./register')
const { login } = require('./login')
const { resetPassword } = require('./reset-password')

router.get('/', (req, res) => {
    res.redirect('./login');
});



module.exports = [register, login, resetPassword, router];