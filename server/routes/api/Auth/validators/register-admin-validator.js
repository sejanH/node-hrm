'use strict';

const { check } = require('express-validator');
module.exports = [
    check('email').not().isEmpty().isEmail(),
    check('password').not().isEmpty().withMessage('Password is required').isLength({ min: 6 }).withMessage('Min length is 6'),
    check('first_name').not().isEmpty().withMessage("First Name Required!"),
    check('last_name').not().isEmpty().withMessage("Last Name Required!"),
    // check('salary_scale').not().isEmpty().withMessage("Salary Required!")
];