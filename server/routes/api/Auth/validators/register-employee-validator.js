'use strict';

const { check } = require('express-validator');
module.exports = [
    //employee Table
    check('email').not().isEmpty().isEmail(),
    // check('emp_id').not().isEmpty(),
    // check('password').not().isEmpty().withMessage('Password is required').isLength({ min: 6 }).withMessage('Min length is 6'),
    check('DepartmentId').not().isEmpty(),
    //employee Info Table
    check('first_name').not().isEmpty(),
    check('last_name').not().isEmpty(),
    check('join_date').not().isEmpty(),
    check('probation_period').not().isEmpty(),
    check('salary_scale').not().isEmpty(),
    check('work_days').not().isEmpty(),
    // check('phone').not().isEmpty(),

];