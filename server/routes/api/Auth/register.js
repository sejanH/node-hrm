require('dotenv').config();
const express = require('express');
const bcrypt = require('bcryptjs');
const passport = require('passport');
const { validationResult } = require('express-validator');
const router = express.Router();

/* Model import */
const Model = require('../../../models/index');

// // Multer
const multer = require("multer");
const upload = require("../../../multer/multer");

/* Validation of form inputs */
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../../utils/response');
const registerAdminValidation = require('./validators/register-admin-validator');
const registerEmployeeValidation = require('./validators/register-employee-validator');

/******************
 @route POST api/auth/register-admin
 @desc register an admin
 @access public 
*********************/
router.post('/register-admin', registerAdminValidation, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    const { email, password, first_name, last_name } = req.body;
    await Model.Employee.findOne({
        where: {
            email
        }
    }).then(emp => {
        if (emp) {
            return res.status(422).json('Email already exists!!');
        }
        var newEmp = new Model.Employee({
            email,
            password,
            role_status: 1
        });
        bcrypt.genSalt(parseInt(process.env.SALT) || 10, (err, salt) => {
            bcrypt.hash(password, salt, (err, hash) => {
                if (err) throw err;
                else {
                    newEmp.password = hash;
                    newEmp.save().then(response => {
                        Model.EmployeeInfo.create({
                            EmployeeId: response.dataValues.id,
                            first_name,
                            last_name
                        }).then(admin => {
                            return res.status(201).json({ msg: "Admin Create Successfully!" })
                        })
                    });
                }
            });
        });
    });
});

/******************
 @route POST api/auth/register-employee
 @desc register an Employee
 @access public 
*********************/
router.post('/register-employee', passport.authenticate("jwt", { session: false }), registerEmployeeValidation, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    if (req.user.role_status == 1) {

        const { email, emp_id, DepartmentId, join_date, probation_period, salary_scale, work_days, phone, first_name, last_name } = req.body;

        await Model.Employee.findOne({
            where: {
                email
            }
        }).then(emp => {
            if (emp) {
                return errorMessageWithData(res, 422, 'error', 'Email already exists!!');
            }
            var newEmp = new Model.Employee({
                email,
                password: "123456",
                role_status: 0,
                emp_id,
                DepartmentId,
                status: 1
            });
            bcrypt.genSalt(parseInt(process.env.SALT) || 10, (err, salt) => {
                bcrypt.hash(newEmp.password, salt, (err, hash) => {
                    if (err) throw err;
                    else {
                        newEmp.password = hash;
                        newEmp.save().then(response => {
                            Model.EmployeeIdMachineIdMap.create({
                                emp_id: response.id
                            }).catch(err => {
                                console.log(err);
                            });
                            Model.EmployeeInfo.create({
                                EmployeeId: response.id,
                                first_name,
                                last_name,
                                join_date,
                                probation_period,
                                salary_scale,
                                work_days,
                                phone
                            }).then(emp => {
                                console.log(emp);
                                return res.status(201).json({ msg: "Employee Create Successfully!" })
                            })
                        });
                    }
                });
            });
        });
    } else {
        return res.status(401).json({ error: "UnAuthorized" })
    }


});

module.exports.register = router;