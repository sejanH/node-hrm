const express = require('express');
const { check, validationResult } = require('express-validator');
const uuid = require("uuid/v4");
const nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');

/* Response Wrapper */
const { successMessage, successMessageWithData, errorMessage, errorMessageWithData } = require('../../../utils/response');
/* Model import */
const Model = require('../../../models/index');

const router = express.Router();



router.post('/reset-password', [check('email').isEmail()], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    Model.Employee.findOne({
        where: {
            email: req.body.email
        }
    }).then(emp => {
        if (emp) {
            let inaAt = Date.now();
            let expAt = Date.now() + 3600000;
            let slug = uuid();

            let transport = nodemailer.createTransport({
                host: "smtp.mailtrap.io",
                port: 2525,
                auth: {
                    user: "bd2ded102313b0",
                    pass: "23bd1a8c6c03e3"
                }
            });

            let mailOptions = {
                from: 'info.jmc@gmail.com',
                to: req.body.email,
                subject: "test",
                text: `<p>Password reset link is </p> <a href='/reset-password/${slug}'>Click Here</a> `
            }
            transport.sendMail(mailOptions, function (err, data) {
                if (err) {
                    console.log(err)
                }
                else {
                    Model.passwordReset.create({
                        email: req.body.email,
                        inaAt,
                        expAt,
                        slug
                    }).then(response => {
                        successMessageWithData(res, 200, "success", response)
                    })


                }
            });
        }

        else
            errorMessage(res, 400, "Invalid Email address")
    })

});

router.post('/reset-password/:slug', [
    check('newPassword', "Password required").not().isEmpty(),
    check('confirmNewPassword', " Confirm password required").not().isEmpty(),

], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    Model.passwordReset.findOne({
        where: {
            slug: req.params.slug
        }
    }).then(response => {
        if (!response) {
            errorMessage(res, 404, "Invalid Request")
        }
        else {
            const { email, inaAt, expAt } = response;
            const { newPassword, confirmNewPassword } = req.body;
            if (expAt > Date.now()) {
                if (newPassword == confirmNewPassword) {
                    bcrypt.genSalt(parseInt(process.env.SALT) || 10, (err, salt) => {
                        bcrypt.hash(newPassword, salt, (err, hash) => {
                            if (err) throw err;
                            else {
                                Model.Employee.update(
                                    { password: hash },
                                    {
                                        where: {
                                            email
                                        }
                                    }
                                ).then(response => {
                                    Model.passwordReset.update({
                                        expAt: Date.now()
                                    }, {
                                        where: {
                                            slug: req.params.slug
                                        }
                                    });
                                    successMessage(res, 200, "Password Updated Successfully")
                                });

                            }
                        });
                    });
                } else {
                    return errorMessage(res, 422, 'passwords don\'t match');
                }
            } else {
                return errorMessage(res, 422, 'reset password token expired');
            }
        }
    })
})

module.exports.resetPassword = router;